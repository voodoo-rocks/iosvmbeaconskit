//
//  iOSVM_Beacons.h
//  iOSVM.Beacons
//
//  Created by Alexander Kryshtalev on 25/05/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMBeacon.h"
#import "VMBeaconManager.h"

@interface VMBeacons : NSObject

+ (NSString *)libraryVersion;

@end
