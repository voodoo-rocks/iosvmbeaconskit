//
//  iOSVM_Beacons.m
//  iOSVM.Beacons
//
//  Created by Alexander Kryshtalev on 25/05/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMBeacons.h"

@implementation VMBeacons

+ (NSString *)libraryVersion;
{
	return @"6.0 beta";
}

@end
