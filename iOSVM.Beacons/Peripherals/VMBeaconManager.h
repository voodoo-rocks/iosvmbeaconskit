//
//  VMiBeaconManager.h
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 04/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "VMSharedManager.h"
#import "VMBeaconBlock.h"
#import "VMBeacon.h"

@interface VMBeaconManager : VMSharedManager <CLLocationManagerDelegate>
{
	VMBeacon *randomBeacon;
	
	NSMutableArray *availableBeacons;
	
	CLBeaconRegion *beaconRegion;
	CLLocationManager *locationManager;
	
	NSString *serviceUuid;
}

@property (copy) VMBeaconBlock didEnterRegion;
@property (copy) VMBeaconBlock didExitRegion;
@property (copy) VMBeaconBlock didDetectDeviceNearby;

@property (retain, nonatomic) NSArray *beacons;

- (VMBeacon *)beaconForMajor:(int)major andMinor:(int)minor;
- (void)scanWithServiceUuidString:(NSString *)serviceUuid;
- (void)scanWithServiceUuidString:(NSString *)serviceUuid andMajor:(NSNumber *)major andMinor:(NSNumber *)minor andIdentifier:(NSString *)string;

- (void)stopScan;

@end
