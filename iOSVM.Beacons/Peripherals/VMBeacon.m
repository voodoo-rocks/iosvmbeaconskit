//
//  VMiBeacon.m
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 04/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMBeacon.h"
#import "VMBeaconManager.h"
#import <CoreLocation/CoreLocation.h>

@implementation VMBeacon

- (void)updateUsingBeacon:(VMBeacon *)beacon;
{
	self.name = beacon.name;
	
	self.major = beacon.major;
	self.minor = beacon.minor;
	
	self.accuracy = beacon.accuracy;
	self.rssi = beacon.rssi;
	self.proximity = beacon.proximity;
	self.proximityUuid = beacon.proximityUuid;
	
	self.hasBeenShown = beacon.hasBeenShown;
	self.isActive = beacon.isActive;
}

- (void)updateUsingCLBeacon:(CLBeacon *)beacon;
{
	self.proximityUuid = beacon.proximityUUID.UUIDString;
	self.major = beacon.major.intValue;
	self.minor = beacon.minor.intValue;
	self.proximity = beacon.proximity;
	self.accuracy = beacon.accuracy;
	self.rssi = beacon.rssi;
}

- (NSString *)proximityDescription;
{
	return [@[@"Unknown", @"Immediate", @"Near", @"Far"] objectAtIndex:self.proximity];
}


@end
