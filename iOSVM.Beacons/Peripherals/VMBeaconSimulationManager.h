//
//  VMiBeaconSimulationManager.h
//  iCardMerchant
//
//  Created by Alexander Kryshtalev on 30/04/14.
//  Copyright (c) 2014 com.voodoo-mobile. All rights reserved.
//

#import "VMBeacon.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "VMSharedManager.h"

@interface VMBeaconSimulationManager : VMSharedManager <CBPeripheralManagerDelegate>
{
	CLBeaconRegion *beaconRegion;
	NSDictionary *beaconData;
	CBPeripheralManager *peripheralManager;
}

- (void)startSimulationForService:(NSString *)serviceUuid withMajor:(int)major andMinor:(int)minor;
- (void)stop;

@end
