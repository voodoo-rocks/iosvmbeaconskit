//
//  VMiBeacon.h
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 04/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "VMBeaconBlock.h"
#import "VMEntity.h"

@class CLBeacon;

@interface VMBeacon : VMEntity

- (void)updateUsingCLBeacon:(CLBeacon *)beacon;
- (void)updateUsingBeacon:(VMBeacon *)beacon;

@property (copy) NSString *proximityUuid;
@property (copy) NSString *name;

@property int major;
@property int minor;

@property float accuracy;
@property NSInteger rssi;
@property NSInteger proximity;

@property (copy) NSArray *userData;
@property BOOL isActive;
@property BOOL hasBeenShown;

@end
