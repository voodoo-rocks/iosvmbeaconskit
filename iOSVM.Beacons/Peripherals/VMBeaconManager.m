//
//  VMiBeaconManager.m
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 04/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMBeaconManager.h"

@implementation VMBeaconManager

- (id)init
{
	self = [super init];
	
	return self;
}

- (void)scanWithServiceUuidString:(NSString *)_serviceUuid andMajor:(NSNumber *)major andMinor:(NSNumber *)minor andIdentifier:(NSString *)string
{
	[self stopScan];
	
	serviceUuid = _serviceUuid;
    
	availableBeacons = [NSMutableArray array];
	locationManager = [[CLLocationManager alloc] init];
	locationManager.delegate = self;
    
    if ([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"] && [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    } else if ([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"] && [locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
	
	NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:serviceUuid];
	if (major && minor) {
		beaconRegion = [[CLBeaconRegion alloc]initWithProximityUUID:uuid major:major.longValue minor:minor.longValue identifier:string];
	} else {
		beaconRegion = [[CLBeaconRegion alloc]initWithProximityUUID:uuid identifier:string];
	}

	beaconRegion.notifyOnEntry = YES;
	beaconRegion.notifyOnExit = YES;
	
 	[locationManager startMonitoringForRegion:beaconRegion];

}

- (void)scanWithServiceUuidString:(NSString *)_serviceUuid;
{
	[self scanWithServiceUuidString:_serviceUuid andMajor:nil andMinor:nil andIdentifier:_serviceUuid];
}

- (void)stopScan
{
	if (beaconRegion) {
		[locationManager stopRangingBeaconsInRegion:beaconRegion];
		[locationManager stopMonitoringForRegion:beaconRegion];
		locationManager.delegate = nil;
		locationManager = nil;
		beaconRegion = nil;
	}
}

- (NSArray *)beacons;
{
	return [availableBeacons copy];
}

- (VMBeacon *)beaconForMajor:(int)major andMinor:(int)minor;
{
	NSArray *array = [availableBeacons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"major = %d and minor = %d", major, minor]];
	return array.firstObject;
}

- (void)locationManager:(CLLocationManager *)manager
		didRangeBeacons:(NSArray *)foundBeacons inRegion:(CLBeaconRegion *)region
{
	NSLog(@"didRangeBeacons, found %d device(s)", (int)foundBeacons.count);
	if (foundBeacons.count > 0 && self.didDetectDeviceNearby) {
		for (CLBeacon *clBeacon in foundBeacons) {
			VMBeacon *beacon = [self beaconForMajor:clBeacon.major.intValue andMinor:clBeacon.minor.intValue];
			
			if (!beacon) {
				beacon = [VMBeacon new];
				[availableBeacons addObject:beacon];
			}
			
			[beacon updateUsingCLBeacon:clBeacon];
			self.didDetectDeviceNearby(self, beacon);
		}
	}
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [locationManager startRangingBeaconsInRegion:beaconRegion];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"didEnterRegion");
	
	if (self.didEnterRegion && [region isKindOfClass:[CLBeaconRegion class]]) {
		CLBeaconRegion *bRegion = (CLBeaconRegion *)region;
		VMBeacon *beacon = [VMBeacon new];
		beacon.major = bRegion.major.intValue;
		beacon.minor = bRegion.minor.intValue;
		self.didEnterRegion(self, beacon);
	}
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{	
	NSLog(@"didExitRegion");
	if (self.didExitRegion && [region isKindOfClass:[CLBeaconRegion class]]) {
		CLBeaconRegion *bRegion = (CLBeaconRegion *)region;
		VMBeacon *beacon = [[VMBeacon alloc] init];
		beacon.major = bRegion.major.intValue;
		beacon.minor = bRegion.minor.intValue;
		self.didExitRegion(self, beacon);
	}
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
	NSLog(@"didDetermineState");
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
	NSLog(@"%@", error.localizedDescription);
}

@end
