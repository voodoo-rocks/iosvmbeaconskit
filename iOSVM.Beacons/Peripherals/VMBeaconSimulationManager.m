//
//  VMiBeaconSimulationManager.m
//  iCardMerchant
//
//  Created by Alexander Kryshtalev on 30/04/14.
//  Copyright (c) 2014 com.voodoo-mobile. All rights reserved.
//

#import "VMBeaconSimulationManager.h"
#import "VMAlertManager.h"

@implementation VMBeaconSimulationManager

- (void)startSimulationForService:(NSString *)serviceUuid withMajor:(int)major andMinor:(int)minor;
{
	NSString *identifier = [NSString stringWithFormat:@"%@/%d-%d", serviceUuid, major, minor];
	beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:serviceUuid]
														   major:major
														   minor:minor
													  identifier:identifier];
	
	beaconData = [beaconRegion peripheralDataWithMeasuredPower:nil];
    peripheralManager = [[CBPeripheralManager alloc]
						 initWithDelegate:self
						 queue:nil
						 options:@{CBCentralManagerOptionShowPowerAlertKey:[NSNumber numberWithBool:YES]}];
	
	if (![CLLocationManager isRangingAvailable]) {
		[[VMAlertManager sharedManager] showInfo:@"Маячки не поддерживаются данным устройством. Пожалуйста, включите bluetooth и попробуйте еще раз"];
	}
}

- (void)stop;
{
	[peripheralManager removeAllServices];
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
	switch (peripheral.state) {
		case CBPeripheralManagerStatePoweredOn:
			[peripheralManager startAdvertising:beaconData];
			break;
		case CBPeripheralManagerStatePoweredOff:
			[peripheralManager stopAdvertising];
			break;
		case CBPeripheralManagerStateUnsupported:
			break;
		default:
			break;
	}}

@end
