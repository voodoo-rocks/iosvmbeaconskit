Pod::Spec.new do |s|
  s.name             = "IOSVMBeaconsKit"
  s.version          = "1.0"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmbeaconskit.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => "public@voodoo-mobile.com" }
  s.source           = { :git => "https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmbeaconskit.git", branch:'master', tag:"1.0"}

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'iOSVM.Beacons/**/*.{h,m}'
  s.resource_bundles = {
    'iOSVM.Beacons' => ['Pod/Assets/*.png']
  }

  s.dependency 'IOSVMCoreKit', '1.0'
  s.public_header_files = 'iOSVM.Beacons/**/*.h'
  
end
